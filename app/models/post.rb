class Post < ApplicationRecord
  validates :title, :body, :rating, presence: true
  validates :rating, numericality: true
  validates :rating, inclusion: 1..5
end
